//
//  AppleAuthManager.swift
//  Shopavize
//
//  Created by Abdul Samad on 10/05/2021.
//

import Foundation
import AuthenticationServices

class AppleAuthManager: NSObject, UINavigationControllerDelegate, ASAuthorizationControllerDelegate{
    
    static let shared = AppleAuthManager()
    var appleLoginCallBack : ((AppleUser) -> ())?
    
    func loginWithApple(_ viewController: UIViewController, _ callback: @escaping ((AppleUser) -> ())) {
        appleLoginCallBack = callback
        
        signInWithApple()
    }
    
    //MARK:- Apple Sign in
    @available(iOS 13.0, *)
    func signInWithApple(){
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let fetchedAppleUser: AppleUser = AppleUser(appleUser: appleIDCredential)
            appleLoginCallBack!(fetchedAppleUser)
            
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("error sign in with apple")
    }
}
