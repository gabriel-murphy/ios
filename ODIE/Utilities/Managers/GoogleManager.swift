//
//  GoogleManager.swift
//  Shopavize
//
//  Created by Abdul Samad on 10/05/2021.
//

import Foundation
import GoogleSignIn

class GoogleManager: NSObject, GIDSignInDelegate, UINavigationControllerDelegate {
    
    static let shared = GoogleManager()
    var googleLoginCallBack : ((GoogleUser) -> ())?;
    
    override init(){
        super.init()
        
    }
    
    // Pick Image from Camera and gallery menu
    func loginWithGoogle(_ viewController: UIViewController, _ callback: @escaping ((GoogleUser) -> ())) {
        googleLoginCallBack = callback
        
        GIDSignIn.sharedInstance()?.presentingViewController = viewController
        GIDSignIn.sharedInstance()?.signIn()
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("We found error \(error.localizedDescription)")
        } else{
            if let googleUser = user{
                let fetchedGoogleUser: GoogleUser = GoogleUser(user: googleUser)
                googleLoginCallBack!(fetchedGoogleUser)
            }
        }
    }
}
