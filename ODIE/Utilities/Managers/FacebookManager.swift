//
//  FacebookManager.swift
//  Shopavize
//
//  Created by Abdul Samad on 10/05/2021.
//

import Foundation
import FBSDKLoginKit

class FBManager: NSObject, UINavigationControllerDelegate {
    
    static let shared = FBManager()
    var facebookLoginCallBack : ((FBUser) -> ())?
    
    
    func loginWithFacebook(_ viewController: UIViewController, _ callback: @escaping ((FBUser) -> ())) {
        facebookLoginCallBack = callback
        
        LoginManager().logIn(permissions: ["email","public_profile"], from: viewController) { (result, err) in
            if err != nil{
                print("Failed")
            } else{
                print(result?.token?.tokenString ?? " ")
                let params = ["fields": "email, first_name, last_name"]
                GraphRequest.init(graphPath: "me", parameters: params).start { (connection, result, err) in
                    if err != nil{
                        print("Failed due to error:", err ?? "")
                        return
                    }
                    let json = JSON(result!)
                    let facebookUser = FBUser(json: json)
                    self.facebookLoginCallBack!(facebookUser)
                    
                }
            }
        }
    }
    
}
