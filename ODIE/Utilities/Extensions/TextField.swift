//
//  TextFields.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor?
        {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setPlaceHolderColor(color:UIColor) {
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
    
    func setupTextFieldExtention(isPreLogin:Bool)
    {
        if isPreLogin{
            self.backgroundColor = .white
        }else{
            self.backgroundColor = .clear
        }
        /* Setting up basic properties of text field*/
        layer.cornerRadius = frame.height / 2
        layer.borderWidth  = 1
    }
    
    /* Input amount only cannot start form 0 */
    func inputAmountOnly(){
        self.keyboardType = .numberPad
        if self.text?.first == "0"{
            self.text = ""
        }
        if !self.text!.isEmpty{
            if  self.text!.last == "0" || self.text!.last == "1" || self.text!.last == "2" || self.text!.last == "3" || self.text!.last == "4" || self.text!.last == "5" || self.text!.last == "6" || self.text!.last == "7" || self.text!.last == "8" || self.text!.last == "9" {
                
            }else{
                self.text?.removeLast()
            }
        }
    }
    
    /* Set CNIC Pattern */
    func inputCnicOnly() {
        
        self.keyboardType = .numberPad
//        if !self.text!.isEmpty{
//            if  self.text!.last == "0" || self.text!.last == "1" || self.text!.last == "2" || self.text!.last == "3" || self.text!.last == "4" || self.text!.last == "5" || self.text!.last == "6" || self.text!.last == "7" || self.text!.last == "8" || self.text!.last == "9" || self.text!.last == "-"{
//
//            }else{
//                self.text?.removeLast()
//            }
//        }

        
        if self.text!.count == 5 || self.text!.count == 13
        {
            self.text?.append("-")
        }else if self.text!.count == 6 || self.text!.count == 14 || self.text?.last == "-"{
            self.text?.removeLast()
            self.text?.removeLast()
        }
    }
    
    /* Input Numbers only can start form 0 */
    func inputNumbersOnly(){
        self.keyboardType = .numberPad
        if !self.text!.isEmpty{
            if  self.text!.last == "0" || self.text!.last == "1" || self.text!.last == "2" || self.text!.last == "3" || self.text!.last == "4" || self.text!.last == "5" || self.text!.last == "6" || self.text!.last == "7" || self.text!.last == "8" || self.text!.last == "9" {

            }else{
                self.text?.removeLast()
            }
        }
    }
}


class StopPasteActionTextField: UITextField {

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
