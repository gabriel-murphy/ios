//
//  StoryBoard.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
// MARK: StoryBoard
extension UIStoryboard {
    
    class func controller<T: UIViewController>() -> T {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: T.className) as! T
    }
}
// MARK: NSObject
extension NSObject {
    class var className: String {
        return String(describing: self.self)
    }
}
