//
//  TableView.swift
//  UPaisa
//
//  Created by MACBOOK on 10/07/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
extension UITableView{
    
    func emptyImageToBg(){
        let imgVu = UIImageView()
        imgVu.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        imgVu.contentMode = .scaleAspectFit
        self.backgroundView = imgVu
    }
    func restore() {
        self.backgroundView = nil
    }
}
