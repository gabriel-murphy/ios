//
//  Label.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
// MARK: Label
extension UILabel{
    
    func setText(text:String,size:CGFloat){
        self.text = text
        self.font = UIFont(name: "Flexo-Regular", size: size)
    }
    
    func setHeaderTitle(text:String){
        self.text = text
        self.textAlignment = .center
        self.font = UIFont(name: "FlexoW01-Bold", size: 22)
    }
    
    func setFormatText(text:String!,isError:Bool,txtFld:UITextField, isPreLogin: Bool? = false){
        self.text = text
        self.textAlignment = .left
        
        if isPreLogin!{
            self.font = UIFont(name: "Flexo-Regular", size: 11)
        }else{
            self.font = UIFont(name: "Flexo-Regular", size: 11)
        }
        
        if isError{
            
        }else{
            
            self.textColor = UIColor.black
        }
        
    }
    
    var isTruncated: Bool {
        
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
    
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}
