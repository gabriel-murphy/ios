//
//  ViewController.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    
    func addLogoToNavigationBarItem(isHomeVC: Bool? = false,ispreLogin:Bool! = false) {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "ic_logo")
        
        //imageView.backgroundColor = .lightGray
        
        // In order to center the title view image no matter what buttons there are, do not set the
        // image view as title view, because it doesn't work. If there is only one button, the image
        // will not be aligned. Instead, a content view is set as title view, then the image view is
        // added as child of the content view. Finally, using constraints the image view is aligned
        // inside its parent.
        
        let leftBarButtonItem = UIBarButtonItem()
        leftBarButtonItem.image = UIImage(named: "leftarrow")
        leftBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        
        leftBarButtonItem.action = #selector(barButtonAction)
        leftBarButtonItem.target = self
        
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: true)
        
        
        let contentView = UIView()
        self.navigationItem.titleView = contentView
        self.navigationItem.titleView?.addSubview(imageView)
        //        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "leftarrow")
        //        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "leftarrow")
        
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "background_1"), for: .default)
        navigationController?.navigationBar.tintColor = .darkGray
    }
    
    
    
    // MARK: Left Bar Button Action
    @objc func barButtonAction() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Navigation Bar methods
    /* Will show the navigation Bar */
    func showNavigationBar()
    {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    /* Will hide the navigation Bar */
    func hideNavigationBar()
    {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

// MARK: Swipe gestures
extension UIViewController{
    
    /* Will enable swipe back gesture from anywhere in screen*/
    func enableSwipeAnywhereInView()
    {
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)
            ))
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    /* Will enable swipe back gesture from left edgeof screen only */
    func enableSwipeBackFromLeftEdge()
    {
        let rightSwipe = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.rightSwipeAction))
        rightSwipe.edges = .left
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    /* Will disable swipe back gesture from left edge of screen only */
    func disableSwipeBackFromLeftEdge()
    {
        let rightSwipe = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.NoSwipeAction(_:)))
        rightSwipe.edges = .left
        self.view.addGestureRecognizer(rightSwipe)
    }
    
    @objc func NoSwipeAction(_ recognizer: UIScreenEdgePanGestureRecognizer)
    {
    }
    
    @objc func rightSwipeAction(_ recognizer: UIScreenEdgePanGestureRecognizer)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleGesture(gesture : UISwipeGestureRecognizer) -> Void
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func openSideMenuWithSwipeGesture(gesture : UISwipeGestureRecognizer) -> Void
    {
        
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    // MARK: Notification
    func sendNotification(title:String,message:String!){
        // 1
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        
        // 3
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
        
        // 4
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func pushViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension UINavigationController {
    func getViewController<T: UIViewController>(of type: T.Type) -> UIViewController? {
        return self.viewControllers.first(where: { $0 is T })
    }
    
    func popToViewController<T: UIViewController>(of type: T.Type, animated: Bool) {
        guard let viewController = self.getViewController(of: type) else { return }
        self.popToViewController(viewController, animated: animated)
    }
    
    
    
    func addContainerView(_ viewController: UIViewController, view: UIView? = nil) {
        guard let targetView = view ?? viewController.view else { return }
        addChild(viewController)
        self.view.addSubview(targetView)
        viewController.didMove(toParent: self)
    }
}
