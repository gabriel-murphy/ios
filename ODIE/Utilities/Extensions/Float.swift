//
//  Float.swift
//  UPaisa
//
//  Created by Macbook pro on 17/06/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation

extension Float {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
