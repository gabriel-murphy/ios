//
//  NotificationNames.swift
//  UPaisa
//
//  Created by MACBOOK on 10/07/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
extension NSNotification.Name {
    public static let Logout: NSNotification.Name = NSNotification.Name(rawValue: "Logout")
}
