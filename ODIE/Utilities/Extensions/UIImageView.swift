//
//  ImageView.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

// MARK: - UIImageView Extension
extension UIImageView {
    // Download Image from URL
    func downloadImage(imageUrl : String!) {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = self.center
        self.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        let url = URL(string: Apis.imageBaseURL + imageUrl)
        self.sd_setImage(with: url, completed: { (image, err, type,url) in
            
            if err != nil
            {
                self.sd_imageIndicator?.stopAnimatingIndicator()
                print("Failed to download image",err?.localizedDescription as Any)
            }
            self.sd_imageIndicator?.stopAnimatingIndicator()
            activityIndicator.stopAnimating()
        })
    }
}


extension UIImage{
    
    func convertUIImageToBase64() -> String
    {
        let imageData = self.jpegData(compressionQuality: 0.2)
        return imageData!.base64EncodedString(options: .lineLength64Characters)
    }
    
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
