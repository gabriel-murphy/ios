//
//  Date.swift
//  UPaisa
//
//  Created by MACBOOK on 14/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation

extension Date {
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
    
    func getDateUsingCustomFormat(format:String!) -> String{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dat = self
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = format
        
        return dateFormatter2.string(from: dat)
    }
}
