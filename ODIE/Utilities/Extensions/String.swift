//
//  String.swift
//  UPaisa
//
//  Created by MACBOOK on 12/05/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation
import UIKit
import CommonCrypto
extension String
{
    func hmac(key: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), key, key.count, self, self.count, &digest)
        let data = Data(bytes: digest)
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var html2Attributed: NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    var isBackspacePressed: Bool {
        let char = self.cString(using: String.Encoding.utf8)!
        return strcmp(char, "\\b") == -92
    }
    
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
    
    func convertBase64ToUIImage() -> UIImage
    {
        let decodedData = Data(base64Encoded: self, options: .ignoreUnknownCharacters)!
        let image = UIImage(data: decodedData)!
        return image
    }
    
    func replaceString(fromString:String, toString:String) -> String{
        return self.replacingOccurrences(of: fromString, with: toString)
    }
    
}


// MARK: Reg Expressions
enum ValidityType
{
    case contactNum
    case email
    case CNIC
    case FavName
    case MailingAddress
    case Amount
    case IBAN
    case ReferenceID
}

extension String{
    func isValid(_ validityType : ValidityType) -> Bool
    {
        let format = "SELF MATCHES %@"
        var regex = ""
        
        switch validityType
        {
        case .email:
            regex = "^[A-Za-z\\d]+([-_.][A-Za-z\\d]+)*@([A-Za-z\\d]+[-.])+[A-Za-z\\d]{2,4}$"

        case .contactNum:
            regex = "^[+]{1}[1]{1} [(]{1}[0-9]{3}[)]{1} [0-9+]{3}-[0-9]{4}$"
            
        case .CNIC:
            regex = "^[0-9]{5}-[0-9+]{7}-[0-9]{1}$"
            
        case .FavName:
            regex = "^[a-zA-Z0-9 ]*$"
        case .MailingAddress:
            regex = "[#.0-9a-zA-Z/s,-/ ]+"
        case .Amount:
            regex = "^[1-9][0-9]*$"
        case .IBAN:
            regex = "^[a-zA-Z0-9]+$"
        case .ReferenceID:
            regex = "^[a-zA-Z0-9]+$"
        }
        
        return NSPredicate(format: format, regex).evaluate(with: self)
    }
    
    /// mask example: `+X (XXX) XXX-XXXX`
    func format(with mask: String) -> String {
        let numbers = self.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        var result = ""
        var index = numbers.startIndex // numbers iterator

        // iterate over the mask characters until the iterator of numbers ends
        for ch in mask where index < numbers.endIndex {
            if ch == "X" {
                // mask requires a number in this place, so take the next one
                result.append(numbers[index])

                // move numbers iterator to the next index
                index = numbers.index(after: index)

            } else {
                result.append(ch) // just append a mask character
            }
        }
        return result
    }
}


// MARK: NSMutable Strring
extension NSMutableAttributedString {
    
    func bold(text:String,size: CGFloat) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: "FlexoW01-Bold", size: size) ?? UIFont.boldSystemFont(ofSize: size)
        ]
        
        self.append(NSAttributedString(string: text, attributes:attributes))
        return self
    }
    
    func normal(text:String,size:CGFloat) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont(name: "Flexo-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
        ]
        
        self.append(NSAttributedString(string: text, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  UIFont(name: "Flexo-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14),
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    
}
