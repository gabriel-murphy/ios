//
//  UIImage.swift
//  ODIE
//
//  Created by Abdul Samad on 13/07/2021.
//

import Foundation
import UIKit

extension UIImage{
    
    class var usernameDeselect:UIImage{
        return UIImage(named: "Username-Deselect")!
    }
    class var usernameSelect:UIImage{
        return UIImage(named: "Username-Select")!
    }
    
    class var passwordDeselect:UIImage{
        return UIImage(named: "Password-Deselct")!
    }
    class var passwordSelect:UIImage{
        return UIImage(named: "Password-Select")!
    }
}
