//
//  UIButton.swift
//  Shopavize
//
//  Created by Abdul Samad on 10/05/2021.
//

import Foundation
import UIKit

extension UIButton{
    
    func setGradientBackground(colors: [UIColor]) {
        self.setGradientBackgroundColor(colors: colors, axis: .vertical, cornerRadius: 0) { view in
            guard let btn = view as? UIButton, let imageView = btn.imageView else { return }
            btn.bringSubviewToFront(imageView) // To display imageview of button
        }
    }
}
