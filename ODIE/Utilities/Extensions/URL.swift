//
//  URL.swift
//  UPaisa
//
//  Created by Macbook pro on 16/06/2020.
//  Copyright © 2020 Zeeshan Ashraf. All rights reserved.
//

import Foundation

extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
