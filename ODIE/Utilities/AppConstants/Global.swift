//
//  Global.swift
//  ODIE
//
//  Created by Abdul Samad on 27/06/2021.
//

import Foundation

class Global {
    class var shared : Global {
        struct Static {
            static let instance : Global = Global()
        }
        return Static.instance
    }
    
    
}
