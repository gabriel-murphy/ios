//
//  AppleUser.swift
//  Shopavize
//
//  Created by Abdul Samad on 18/05/2021.
//

import Foundation
import AuthenticationServices

class AppleUser {
    
    var userId: String = ""
    var fullName: String = ""
    var email: String = ""
    
    init(appleUser: ASAuthorizationAppleIDCredential) {
        userId = appleUser.user
        fullName = String(describing: appleUser.fullName)
        email = appleUser.email ?? ""
    }
}
