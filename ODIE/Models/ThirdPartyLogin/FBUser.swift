//
//  FBUser.swift
//  Shopavize
//
//  Created by Abdul Samad on 10/05/2021.
//

import Foundation

class FBUser {
    
    var id: String = ""
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    
    init(json: JSON) {
        id = json["id"].stringValue
        email = json["email"].stringValue
        firstName = json["first_name"].stringValue
        lastName = json["last_name"].stringValue
    }
}
