//
//  GoogleUser.swift
//  Shopavize
//
//  Created by Abdul Samad on 17/05/2021.
//

import Foundation
import GoogleSignIn

class GoogleUser {
    
    var email: String = ""
    var userName: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var userID: String = ""
    
    init(user: GIDGoogleUser) {
        email = user.profile?.email ?? ""
        userName = user.profile?.name ?? ""
        firstName = user.profile?.givenName ?? ""
        lastName = user.profile?.familyName ?? ""
        userID = user.userID ?? ""
    }
}
